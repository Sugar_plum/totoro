const app = document.getElementById('root');

const logo = document.createElement('img');
logo.src = 'logo.png';
logo.alt = 'Totoro';

const container = document.createElement('div');
container.setAttribute('class', 'container');

app.appendChild(logo);
app.appendChild(container);


fetch('https://ghibliapi.herokuapp.com/films')
  .then((response) => {
    return response.json()
  })
  .then((data) => {
    data.forEach((movie) => {
      const movieCard = document.createElement('article');
      movieCard.setAttribute('class', 'card');

      const movieTitle = document.createElement('h1');
      movieTitle.textContent = movie.title;

      const movieDescr = document.createElement('p');
      movieDescr.textContent = movie.description;

      container.appendChild(movieCard);
      movieCard.appendChild(movieTitle);
      movieCard.appendChild(movieDescr);

    })
  })
  .catch((err) => {
    const errorMessage = document.createElement("span");
    errorMessage.innerHTML = "Oh, no! We have no data. <br> Please, try again";
    app.appendChild(errorMessage);
  })